
package com.huawei.evcinterface.portalmgrmsg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.evcinterface.common.RequestHeader;
import com.huawei.evcinterface.portalmgr.QueryTransactionRequest;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://www.huawei.com/evcinterface/common}RequestHeader"/>
 *         &lt;element name="QueryTransactionRequest" type="{http://www.huawei.com/evcinterface/portalmgr}QueryTransactionRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "queryTransactionRequest"
})
@XmlRootElement(name = "QueryTransactionRequestMsg")
public class QueryTransactionRequestMsg {

    @XmlElement(name = "RequestHeader", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "QueryTransactionRequest", required = true)
    protected QueryTransactionRequest queryTransactionRequest;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the queryTransactionRequest property.
     * 
     * @return
     *     possible object is
     *     {@link QueryTransactionRequest }
     *     
     */
    public QueryTransactionRequest getQueryTransactionRequest() {
        return queryTransactionRequest;
    }

    /**
     * Sets the value of the queryTransactionRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryTransactionRequest }
     *     
     */
    public void setQueryTransactionRequest(QueryTransactionRequest value) {
        this.queryTransactionRequest = value;
    }

}
