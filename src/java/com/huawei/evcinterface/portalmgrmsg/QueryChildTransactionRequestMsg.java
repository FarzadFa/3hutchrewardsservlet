
package com.huawei.evcinterface.portalmgrmsg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.evcinterface.common.RequestHeader;
import com.huawei.evcinterface.portalmgr.QueryChildTransactionRequest;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://www.huawei.com/evcinterface/common}RequestHeader"/>
 *         &lt;element name="QueryChildTransactionRequest" type="{http://www.huawei.com/evcinterface/portalmgr}QueryChildTransactionRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "queryChildTransactionRequest"
})
@XmlRootElement(name = "QueryChildTransactionRequestMsg")
public class QueryChildTransactionRequestMsg {

    @XmlElement(name = "RequestHeader", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "QueryChildTransactionRequest", required = true)
    protected QueryChildTransactionRequest queryChildTransactionRequest;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the queryChildTransactionRequest property.
     * 
     * @return
     *     possible object is
     *     {@link QueryChildTransactionRequest }
     *     
     */
    public QueryChildTransactionRequest getQueryChildTransactionRequest() {
        return queryChildTransactionRequest;
    }

    /**
     * Sets the value of the queryChildTransactionRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryChildTransactionRequest }
     *     
     */
    public void setQueryChildTransactionRequest(QueryChildTransactionRequest value) {
        this.queryChildTransactionRequest = value;
    }

}
