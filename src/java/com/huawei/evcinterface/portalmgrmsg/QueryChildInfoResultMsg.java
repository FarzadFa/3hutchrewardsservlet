
package com.huawei.evcinterface.portalmgrmsg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.evcinterface.common.ResultHeader;
import com.huawei.evcinterface.portalmgr.QueryChildInfoResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultHeader" type="{http://www.huawei.com/evcinterface/common}ResultHeader"/>
 *         &lt;element name="QueryChildInfoResult" type="{http://www.huawei.com/evcinterface/portalmgr}QueryChildInfoResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultHeader",
    "queryChildInfoResult"
})
@XmlRootElement(name = "QueryChildInfoResultMsg")
public class QueryChildInfoResultMsg {

    @XmlElement(name = "ResultHeader", required = true)
    protected ResultHeader resultHeader;
    @XmlElement(name = "QueryChildInfoResult", required = true)
    protected QueryChildInfoResult queryChildInfoResult;

    /**
     * Gets the value of the resultHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResultHeader }
     *     
     */
    public ResultHeader getResultHeader() {
        return resultHeader;
    }

    /**
     * Sets the value of the resultHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultHeader }
     *     
     */
    public void setResultHeader(ResultHeader value) {
        this.resultHeader = value;
    }

    /**
     * Gets the value of the queryChildInfoResult property.
     * 
     * @return
     *     possible object is
     *     {@link QueryChildInfoResult }
     *     
     */
    public QueryChildInfoResult getQueryChildInfoResult() {
        return queryChildInfoResult;
    }

    /**
     * Sets the value of the queryChildInfoResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryChildInfoResult }
     *     
     */
    public void setQueryChildInfoResult(QueryChildInfoResult value) {
        this.queryChildInfoResult = value;
    }

}
