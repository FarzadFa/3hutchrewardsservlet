
package com.huawei.evcinterface.portalmgrmsg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.evcinterface.common.ResultHeader;
import com.huawei.evcinterface.portalmgr.SellStockResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SellStockResult" type="{http://www.huawei.com/evcinterface/portalmgr}SellStockResult"/>
 *         &lt;element name="ResultHeader" type="{http://www.huawei.com/evcinterface/common}ResultHeader"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sellStockResult",
    "resultHeader"
})
@XmlRootElement(name = "SellStockResultMsg")
public class SellStockResultMsg {

    @XmlElement(name = "SellStockResult", required = true)
    protected SellStockResult sellStockResult;
    @XmlElement(name = "ResultHeader", required = true)
    protected ResultHeader resultHeader;

    /**
     * Gets the value of the sellStockResult property.
     * 
     * @return
     *     possible object is
     *     {@link SellStockResult }
     *     
     */
    public SellStockResult getSellStockResult() {
        return sellStockResult;
    }

    /**
     * Sets the value of the sellStockResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link SellStockResult }
     *     
     */
    public void setSellStockResult(SellStockResult value) {
        this.sellStockResult = value;
    }

    /**
     * Gets the value of the resultHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResultHeader }
     *     
     */
    public ResultHeader getResultHeader() {
        return resultHeader;
    }

    /**
     * Sets the value of the resultHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultHeader }
     *     
     */
    public void setResultHeader(ResultHeader value) {
        this.resultHeader = value;
    }

}
