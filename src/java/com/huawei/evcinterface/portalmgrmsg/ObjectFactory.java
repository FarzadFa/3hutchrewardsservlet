
package com.huawei.evcinterface.portalmgrmsg;

import javax.xml.bind.annotation.XmlRegistry;
import com.huawei.evcinterface.portalmgrmsg.C2CTransferResultMsg;
import com.huawei.evcinterface.portalmgrmsg.ChangeLoginPwdResultMsg;
import com.huawei.evcinterface.portalmgrmsg.ChangePINRequestMsg;
import com.huawei.evcinterface.portalmgrmsg.ConfirmSellStockRequestMsg;
import com.huawei.evcinterface.portalmgrmsg.ConfirmSellStockResultMsg;
import com.huawei.evcinterface.portalmgrmsg.CreateChildDealerResultMsg;
import com.huawei.evcinterface.portalmgrmsg.DeactivateChildResultMsg;
import com.huawei.evcinterface.portalmgrmsg.ModifyLanguageResultMsg;
import com.huawei.evcinterface.portalmgrmsg.QueryChildInfoResultMsg;
import com.huawei.evcinterface.portalmgrmsg.QueryChildInventoryResultMsg;
import com.huawei.evcinterface.portalmgrmsg.QueryChildTransactionResultMsg;
import com.huawei.evcinterface.portalmgrmsg.QueryDealerInfoResultMsg;
import com.huawei.evcinterface.portalmgrmsg.QueryInventoryResultMsg;
import com.huawei.evcinterface.portalmgrmsg.QueryTransactionResultMsg;
import com.huawei.evcinterface.portalmgrmsg.ResetChildPINResultMsg;
import com.huawei.evcinterface.portalmgrmsg.SellStockResultMsg;
import com.huawei.evcinterface.portalmgrmsg.TransactionAuditRequestMsg;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.huawei.evcinterface.portalmgrmsg package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.huawei.evcinterface.portalmgrmsg
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link QueryInventoryResultMsg }
     * 
     */
    public QueryInventoryResultMsg createQueryInventoryResultMsg() {
        return new QueryInventoryResultMsg();
    }

    /**
     * Create an instance of {@link QueryTransactionResultMsg }
     * 
     */
    public QueryTransactionResultMsg createQueryTransactionResultMsg() {
        return new QueryTransactionResultMsg();
    }

    /**
     * Create an instance of {@link ConfirmSellStockResultMsg }
     * 
     */
    public ConfirmSellStockResultMsg createConfirmSellStockResultMsg() {
        return new ConfirmSellStockResultMsg();
    }

    /**
     * Create an instance of {@link ChangeLoginPwdRequestMsg }
     * 
     */
    public ChangeLoginPwdRequestMsg createChangeLoginPwdRequestMsg() {
        return new ChangeLoginPwdRequestMsg();
    }

    /**
     * Create an instance of {@link ChangeLoginPwdResultMsg }
     * 
     */
    public ChangeLoginPwdResultMsg createChangeLoginPwdResultMsg() {
        return new ChangeLoginPwdResultMsg();
    }

    /**
     * Create an instance of {@link QueryChildInfoRequestMsg }
     * 
     */
    public QueryChildInfoRequestMsg createQueryChildInfoRequestMsg() {
        return new QueryChildInfoRequestMsg();
    }

    /**
     * Create an instance of {@link QueryChildInventoryResultMsg }
     * 
     */
    public QueryChildInventoryResultMsg createQueryChildInventoryResultMsg() {
        return new QueryChildInventoryResultMsg();
    }

    /**
     * Create an instance of {@link PrepaidRechargeResultMsg }
     * 
     */
    public PrepaidRechargeResultMsg createPrepaidRechargeResultMsg() {
        return new PrepaidRechargeResultMsg();
    }

    /**
     * Create an instance of {@link QueryDealerInfoRequestMsg }
     * 
     */
    public QueryDealerInfoRequestMsg createQueryDealerInfoRequestMsg() {
        return new QueryDealerInfoRequestMsg();
    }

    /**
     * Create an instance of {@link ConfirmSellStockRequestMsg }
     * 
     */
    public ConfirmSellStockRequestMsg createConfirmSellStockRequestMsg() {
        return new ConfirmSellStockRequestMsg();
    }

    /**
     * Create an instance of {@link TransactionAuditRequestMsg }
     * 
     */
    public TransactionAuditRequestMsg createTransactionAuditRequestMsg() {
        return new TransactionAuditRequestMsg();
    }

    /**
     * Create an instance of {@link ChangePINResultMsg }
     * 
     */
    public ChangePINResultMsg createChangePINResultMsg() {
        return new ChangePINResultMsg();
    }

    /**
     * Create an instance of {@link CreateChildDealerResultMsg }
     * 
     */
    public CreateChildDealerResultMsg createCreateChildDealerResultMsg() {
        return new CreateChildDealerResultMsg();
    }

    /**
     * Create an instance of {@link ChangePINRequestMsg }
     * 
     */
    public ChangePINRequestMsg createChangePINRequestMsg() {
        return new ChangePINRequestMsg();
    }

    /**
     * Create an instance of {@link QueryChildTransactionResultMsg }
     * 
     */
    public QueryChildTransactionResultMsg createQueryChildTransactionResultMsg() {
        return new QueryChildTransactionResultMsg();
    }

    /**
     * Create an instance of {@link QueryChildInfoResultMsg }
     * 
     */
    public QueryChildInfoResultMsg createQueryChildInfoResultMsg() {
        return new QueryChildInfoResultMsg();
    }

    /**
     * Create an instance of {@link DeactivateChildResultMsg }
     * 
     */
    public DeactivateChildResultMsg createDeactivateChildResultMsg() {
        return new DeactivateChildResultMsg();
    }

    /**
     * Create an instance of {@link ModifyLanguageResultMsg }
     * 
     */
    public ModifyLanguageResultMsg createModifyLanguageResultMsg() {
        return new ModifyLanguageResultMsg();
    }

    /**
     * Create an instance of {@link QueryDealerInfoResultMsg }
     * 
     */
    public QueryDealerInfoResultMsg createQueryDealerInfoResultMsg() {
        return new QueryDealerInfoResultMsg();
    }

    /**
     * Create an instance of {@link QueryTransactionRequestMsg }
     * 
     */
    public QueryTransactionRequestMsg createQueryTransactionRequestMsg() {
        return new QueryTransactionRequestMsg();
    }

    /**
     * Create an instance of {@link SellStockResultMsg }
     * 
     */
    public SellStockResultMsg createSellStockResultMsg() {
        return new SellStockResultMsg();
    }

    /**
     * Create an instance of {@link PrepaidRechargeRequestMsg }
     * 
     */
    public PrepaidRechargeRequestMsg createPrepaidRechargeRequestMsg() {
        return new PrepaidRechargeRequestMsg();
    }

    /**
     * Create an instance of {@link CreateChildDealerRequestMsg }
     * 
     */
    public CreateChildDealerRequestMsg createCreateChildDealerRequestMsg() {
        return new CreateChildDealerRequestMsg();
    }

    /**
     * Create an instance of {@link TransactionAuditResultMsg }
     * 
     */
    public TransactionAuditResultMsg createTransactionAuditResultMsg() {
        return new TransactionAuditResultMsg();
    }

    /**
     * Create an instance of {@link QueryChildTransactionRequestMsg }
     * 
     */
    public QueryChildTransactionRequestMsg createQueryChildTransactionRequestMsg() {
        return new QueryChildTransactionRequestMsg();
    }

    /**
     * Create an instance of {@link C2CTransferRequestMsg }
     * 
     */
    public C2CTransferRequestMsg createC2CTransferRequestMsg() {
        return new C2CTransferRequestMsg();
    }

    /**
     * Create an instance of {@link ResetChildPINRequestMsg }
     * 
     */
    public ResetChildPINRequestMsg createResetChildPINRequestMsg() {
        return new ResetChildPINRequestMsg();
    }

    /**
     * Create an instance of {@link DeactivateChildRequestMsg }
     * 
     */
    public DeactivateChildRequestMsg createDeactivateChildRequestMsg() {
        return new DeactivateChildRequestMsg();
    }

    /**
     * Create an instance of {@link ResetChildPINResultMsg }
     * 
     */
    public ResetChildPINResultMsg createResetChildPINResultMsg() {
        return new ResetChildPINResultMsg();
    }

    /**
     * Create an instance of {@link SellStockRequestMsg }
     * 
     */
    public SellStockRequestMsg createSellStockRequestMsg() {
        return new SellStockRequestMsg();
    }

    /**
     * Create an instance of {@link ModifyLanguageRequestMsg }
     * 
     */
    public ModifyLanguageRequestMsg createModifyLanguageRequestMsg() {
        return new ModifyLanguageRequestMsg();
    }

    /**
     * Create an instance of {@link C2CTransferResultMsg }
     * 
     */
    public C2CTransferResultMsg createC2CTransferResultMsg() {
        return new C2CTransferResultMsg();
    }

    /**
     * Create an instance of {@link QueryInventoryRequestMsg }
     * 
     */
    public QueryInventoryRequestMsg createQueryInventoryRequestMsg() {
        return new QueryInventoryRequestMsg();
    }

    /**
     * Create an instance of {@link QueryChildInventoryRequestMsg }
     * 
     */
    public QueryChildInventoryRequestMsg createQueryChildInventoryRequestMsg() {
        return new QueryChildInventoryRequestMsg();
    }

}
