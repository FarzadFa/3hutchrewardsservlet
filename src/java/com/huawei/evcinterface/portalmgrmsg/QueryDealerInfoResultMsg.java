
package com.huawei.evcinterface.portalmgrmsg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.evcinterface.common.ResultHeader;
import com.huawei.evcinterface.portalmgr.QueryDealerInfoResult;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ResultHeader" type="{http://www.huawei.com/evcinterface/common}ResultHeader"/>
 *         &lt;element name="QueryDealerInfoResult" type="{http://www.huawei.com/evcinterface/portalmgr}QueryDealerInfoResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "resultHeader",
    "queryDealerInfoResult"
})
@XmlRootElement(name = "QueryDealerInfoResultMsg")
public class QueryDealerInfoResultMsg {

    @XmlElement(name = "ResultHeader", required = true)
    protected ResultHeader resultHeader;
    @XmlElement(name = "QueryDealerInfoResult", required = true)
    protected QueryDealerInfoResult queryDealerInfoResult;

    /**
     * Gets the value of the resultHeader property.
     * 
     * @return
     *     possible object is
     *     {@link ResultHeader }
     *     
     */
    public ResultHeader getResultHeader() {
        return resultHeader;
    }

    /**
     * Sets the value of the resultHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultHeader }
     *     
     */
    public void setResultHeader(ResultHeader value) {
        this.resultHeader = value;
    }

    /**
     * Gets the value of the queryDealerInfoResult property.
     * 
     * @return
     *     possible object is
     *     {@link QueryDealerInfoResult }
     *     
     */
    public QueryDealerInfoResult getQueryDealerInfoResult() {
        return queryDealerInfoResult;
    }

    /**
     * Sets the value of the queryDealerInfoResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link QueryDealerInfoResult }
     *     
     */
    public void setQueryDealerInfoResult(QueryDealerInfoResult value) {
        this.queryDealerInfoResult = value;
    }

}
