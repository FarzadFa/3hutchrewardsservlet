
package com.huawei.evcinterface.portalmgrmsg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.evcinterface.common.RequestHeader;
import com.huawei.evcinterface.portalmgr.C2CTransferRequest;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://www.huawei.com/evcinterface/common}RequestHeader"/>
 *         &lt;element name="C2CTransferRequest" type="{http://www.huawei.com/evcinterface/portalmgr}C2CTransferRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "c2CTransferRequest"
})
@XmlRootElement(name = "C2CTransferRequestMsg")
public class C2CTransferRequestMsg {

    @XmlElement(name = "RequestHeader", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "C2CTransferRequest", required = true)
    protected C2CTransferRequest c2CTransferRequest;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the c2CTransferRequest property.
     * 
     * @return
     *     possible object is
     *     {@link C2CTransferRequest }
     *     
     */
    public C2CTransferRequest getC2CTransferRequest() {
        return c2CTransferRequest;
    }

    /**
     * Sets the value of the c2CTransferRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link C2CTransferRequest }
     *     
     */
    public void setC2CTransferRequest(C2CTransferRequest value) {
        this.c2CTransferRequest = value;
    }

}
