
package com.huawei.evcinterface.portalmgrmsg;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.huawei.evcinterface.common.RequestHeader;
import com.huawei.evcinterface.portalmgr.PrepaidRechargeRequest;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestHeader" type="{http://www.huawei.com/evcinterface/common}RequestHeader"/>
 *         &lt;element name="PrepaidRechargeRequest" type="{http://www.huawei.com/evcinterface/portalmgr}PrepaidRechargeRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "requestHeader",
    "prepaidRechargeRequest"
})
@XmlRootElement(name = "PrepaidRechargeRequestMsg")
public class PrepaidRechargeRequestMsg {

    @XmlElement(name = "RequestHeader", required = true)
    protected RequestHeader requestHeader;
    @XmlElement(name = "PrepaidRechargeRequest", required = true)
    protected PrepaidRechargeRequest prepaidRechargeRequest;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RequestHeader }
     *     
     */
    public RequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestHeader }
     *     
     */
    public void setRequestHeader(RequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the prepaidRechargeRequest property.
     * 
     * @return
     *     possible object is
     *     {@link PrepaidRechargeRequest }
     *     
     */
    public PrepaidRechargeRequest getPrepaidRechargeRequest() {
        return prepaidRechargeRequest;
    }

    /**
     * Sets the value of the prepaidRechargeRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrepaidRechargeRequest }
     *     
     */
    public void setPrepaidRechargeRequest(PrepaidRechargeRequest value) {
        this.prepaidRechargeRequest = value;
    }

}
