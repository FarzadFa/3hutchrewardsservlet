
package com.huawei.evcinterface.portalmgr;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TransactionAuditResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TransactionAuditResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TransactionAuditResult" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TransactionTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Transtype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="HPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TOHPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="PriceType" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="InitBalance" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="CurrentBalance" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="Commvalue" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TransactionAuditResult", propOrder = {
    "transactionAuditResult"
})
public class TransactionAuditResult {

    @XmlElement(name = "TransactionAuditResult")
    protected List<TransactionAuditResult.TransactionAuditResults> transactionAuditResult;

    /**
     * Gets the value of the transactionAuditResult property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the transactionAuditResult property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTransactionAuditResult().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TransactionAuditResult.TransactionAuditResult }
     * 
     * 
     */
    public List<TransactionAuditResult.TransactionAuditResults> getTransactionAuditResult() {
        if (transactionAuditResult == null) {
            transactionAuditResult = new ArrayList<TransactionAuditResult.TransactionAuditResults>();
        }
        return this.transactionAuditResult;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TransactionTime" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Transtype" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="HPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TOHPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="PriceType" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="Quantity" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="InitBalance" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="CurrentBalance" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="Commvalue" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "transactionTime",
        "transtype",
        "hpno",
        "tohpno",
        "priceType",
        "quantity",
        "initBalance",
        "currentBalance",
        "commvalue",
        "status"
    })
    public static class TransactionAuditResults {

        @XmlElement(name = "TransactionTime", required = true)
        protected String transactionTime;
        @XmlElement(name = "Transtype", required = true)
        protected String transtype;
        @XmlElement(name = "HPNO", required = true)
        protected String hpno;
        @XmlElement(name = "TOHPNO", required = true)
        protected String tohpno;
        @XmlElement(name = "PriceType")
        protected long priceType;
        @XmlElement(name = "Quantity")
        protected int quantity;
        @XmlElement(name = "InitBalance")
        protected long initBalance;
        @XmlElement(name = "CurrentBalance")
        protected long currentBalance;
        @XmlElement(name = "Commvalue")
        protected long commvalue;
        @XmlElement(name = "Status")
        protected int status;

        /**
         * Gets the value of the transactionTime property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTransactionTime() {
            return transactionTime;
        }

        /**
         * Sets the value of the transactionTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTransactionTime(String value) {
            this.transactionTime = value;
        }

        /**
         * Gets the value of the transtype property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTranstype() {
            return transtype;
        }

        /**
         * Sets the value of the transtype property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTranstype(String value) {
            this.transtype = value;
        }

        /**
         * Gets the value of the hpno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHPNO() {
            return hpno;
        }

        /**
         * Sets the value of the hpno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHPNO(String value) {
            this.hpno = value;
        }

        /**
         * Gets the value of the tohpno property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTOHPNO() {
            return tohpno;
        }

        /**
         * Sets the value of the tohpno property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTOHPNO(String value) {
            this.tohpno = value;
        }

        /**
         * Gets the value of the priceType property.
         * 
         */
        public long getPriceType() {
            return priceType;
        }

        /**
         * Sets the value of the priceType property.
         * 
         */
        public void setPriceType(long value) {
            this.priceType = value;
        }

        /**
         * Gets the value of the quantity property.
         * 
         */
        public int getQuantity() {
            return quantity;
        }

        /**
         * Sets the value of the quantity property.
         * 
         */
        public void setQuantity(int value) {
            this.quantity = value;
        }

        /**
         * Gets the value of the initBalance property.
         * 
         */
        public long getInitBalance() {
            return initBalance;
        }

        /**
         * Sets the value of the initBalance property.
         * 
         */
        public void setInitBalance(long value) {
            this.initBalance = value;
        }

        /**
         * Gets the value of the currentBalance property.
         * 
         */
        public long getCurrentBalance() {
            return currentBalance;
        }

        /**
         * Sets the value of the currentBalance property.
         * 
         */
        public void setCurrentBalance(long value) {
            this.currentBalance = value;
        }

        /**
         * Gets the value of the commvalue property.
         * 
         */
        public long getCommvalue() {
            return commvalue;
        }

        /**
         * Sets the value of the commvalue property.
         * 
         */
        public void setCommvalue(long value) {
            this.commvalue = value;
        }

        /**
         * Gets the value of the status property.
         * 
         */
        public int getStatus() {
            return status;
        }

        /**
         * Sets the value of the status property.
         * 
         */
        public void setStatus(int value) {
            this.status = value;
        }

    }

}
