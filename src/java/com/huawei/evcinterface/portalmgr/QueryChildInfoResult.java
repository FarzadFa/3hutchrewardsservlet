
package com.huawei.evcinterface.portalmgr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QueryChildInfoResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QueryChildInfoResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DLName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="HPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Status" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MainHPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ParentHPNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DLLEVEL" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="EMail" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Address1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Address2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FaxNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ICCID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RegionID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="UpLinkStatus" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Rank" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NoOfChildren" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Language" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Birthday" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BrandID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ModelID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContactName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CommissionClass" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MaxDailyTransfer" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MaxDailyRecharge" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AdminStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PurchaseStockCommissionRate" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DealerID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BANKNAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BANKACCOUNTNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QueryChildInfoResult", propOrder = {
    "dlName",
    "hpno",
    "status",
    "mainHPNO",
    "parentHPNO",
    "dllevel",
    "eMail",
    "address1",
    "address2",
    "city",
    "postalCode",
    "phoneNumber",
    "faxNumber",
    "iccid",
    "regionID",
    "upLinkStatus",
    "rank",
    "noOfChildren",
    "language",
    "birthday",
    "brandID",
    "modelID",
    "contactName",
    "commissionClass",
    "maxDailyTransfer",
    "maxDailyRecharge",
    "adminStatus",
    "purchaseStockCommissionRate",
    "dealerID",
    "bankname",
    "bankaccountno"
})
public class QueryChildInfoResult {

    @XmlElement(name = "DLName", required = true)
    protected String dlName;
    @XmlElement(name = "HPNO", required = true)
    protected String hpno;
    @XmlElement(name = "Status")
    protected int status;
    @XmlElement(name = "MainHPNO", required = true)
    protected String mainHPNO;
    @XmlElement(name = "ParentHPNO", required = true)
    protected String parentHPNO;
    @XmlElement(name = "DLLEVEL")
    protected int dllevel;
    @XmlElement(name = "EMail", required = true)
    protected String eMail;
    @XmlElement(name = "Address1", required = true)
    protected String address1;
    @XmlElement(name = "Address2", required = true)
    protected String address2;
    @XmlElement(name = "City", required = true)
    protected String city;
    @XmlElement(name = "PostalCode", required = true)
    protected String postalCode;
    @XmlElement(name = "PhoneNumber", required = true)
    protected String phoneNumber;
    @XmlElement(name = "FaxNumber", required = true)
    protected String faxNumber;
    @XmlElement(name = "ICCID", required = true)
    protected String iccid;
    @XmlElement(name = "RegionID")
    protected int regionID;
    @XmlElement(name = "UpLinkStatus")
    protected int upLinkStatus;
    @XmlElement(name = "Rank")
    protected int rank;
    @XmlElement(name = "NoOfChildren")
    protected int noOfChildren;
    @XmlElement(name = "Language")
    protected int language;
    @XmlElement(name = "Birthday", required = true)
    protected String birthday;
    @XmlElement(name = "BrandID", required = true)
    protected String brandID;
    @XmlElement(name = "ModelID", required = true)
    protected String modelID;
    @XmlElement(name = "ContactName", required = true)
    protected String contactName;
    @XmlElement(name = "CommissionClass")
    protected int commissionClass;
    @XmlElement(name = "MaxDailyTransfer")
    protected int maxDailyTransfer;
    @XmlElement(name = "MaxDailyRecharge")
    protected int maxDailyRecharge;
    @XmlElement(name = "AdminStatus", required = true)
    protected String adminStatus;
    @XmlElement(name = "PurchaseStockCommissionRate")
    protected int purchaseStockCommissionRate;
    @XmlElement(name = "DealerID", required = true)
    protected String dealerID;
    @XmlElement(name = "BANKNAME")
    protected String bankname;
    @XmlElement(name = "BANKACCOUNTNO")
    protected String bankaccountno;

    /**
     * Gets the value of the dlName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDLName() {
        return dlName;
    }

    /**
     * Sets the value of the dlName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDLName(String value) {
        this.dlName = value;
    }

    /**
     * Gets the value of the hpno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHPNO() {
        return hpno;
    }

    /**
     * Sets the value of the hpno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHPNO(String value) {
        this.hpno = value;
    }

    /**
     * Gets the value of the status property.
     * 
     */
    public int getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     */
    public void setStatus(int value) {
        this.status = value;
    }

    /**
     * Gets the value of the mainHPNO property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainHPNO() {
        return mainHPNO;
    }

    /**
     * Sets the value of the mainHPNO property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainHPNO(String value) {
        this.mainHPNO = value;
    }

    /**
     * Gets the value of the parentHPNO property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParentHPNO() {
        return parentHPNO;
    }

    /**
     * Sets the value of the parentHPNO property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParentHPNO(String value) {
        this.parentHPNO = value;
    }

    /**
     * Gets the value of the dllevel property.
     * 
     */
    public int getDLLEVEL() {
        return dllevel;
    }

    /**
     * Sets the value of the dllevel property.
     * 
     */
    public void setDLLEVEL(int value) {
        this.dllevel = value;
    }

    /**
     * Gets the value of the eMail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMail() {
        return eMail;
    }

    /**
     * Sets the value of the eMail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMail(String value) {
        this.eMail = value;
    }

    /**
     * Gets the value of the address1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Sets the value of the address1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress1(String value) {
        this.address1 = value;
    }

    /**
     * Gets the value of the address2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Sets the value of the address2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress2(String value) {
        this.address2 = value;
    }

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Gets the value of the postalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * Sets the value of the postalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostalCode(String value) {
        this.postalCode = value;
    }

    /**
     * Gets the value of the phoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets the value of the phoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhoneNumber(String value) {
        this.phoneNumber = value;
    }

    /**
     * Gets the value of the faxNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxNumber() {
        return faxNumber;
    }

    /**
     * Sets the value of the faxNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxNumber(String value) {
        this.faxNumber = value;
    }

    /**
     * Gets the value of the iccid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getICCID() {
        return iccid;
    }

    /**
     * Sets the value of the iccid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setICCID(String value) {
        this.iccid = value;
    }

    /**
     * Gets the value of the regionID property.
     * 
     */
    public int getRegionID() {
        return regionID;
    }

    /**
     * Sets the value of the regionID property.
     * 
     */
    public void setRegionID(int value) {
        this.regionID = value;
    }

    /**
     * Gets the value of the upLinkStatus property.
     * 
     */
    public int getUpLinkStatus() {
        return upLinkStatus;
    }

    /**
     * Sets the value of the upLinkStatus property.
     * 
     */
    public void setUpLinkStatus(int value) {
        this.upLinkStatus = value;
    }

    /**
     * Gets the value of the rank property.
     * 
     */
    public int getRank() {
        return rank;
    }

    /**
     * Sets the value of the rank property.
     * 
     */
    public void setRank(int value) {
        this.rank = value;
    }

    /**
     * Gets the value of the noOfChildren property.
     * 
     */
    public int getNoOfChildren() {
        return noOfChildren;
    }

    /**
     * Sets the value of the noOfChildren property.
     * 
     */
    public void setNoOfChildren(int value) {
        this.noOfChildren = value;
    }

    /**
     * Gets the value of the language property.
     * 
     */
    public int getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     */
    public void setLanguage(int value) {
        this.language = value;
    }

    /**
     * Gets the value of the birthday property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * Sets the value of the birthday property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBirthday(String value) {
        this.birthday = value;
    }

    /**
     * Gets the value of the brandID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBrandID() {
        return brandID;
    }

    /**
     * Sets the value of the brandID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBrandID(String value) {
        this.brandID = value;
    }

    /**
     * Gets the value of the modelID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelID() {
        return modelID;
    }

    /**
     * Sets the value of the modelID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelID(String value) {
        this.modelID = value;
    }

    /**
     * Gets the value of the contactName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * Sets the value of the contactName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactName(String value) {
        this.contactName = value;
    }

    /**
     * Gets the value of the commissionClass property.
     * 
     */
    public int getCommissionClass() {
        return commissionClass;
    }

    /**
     * Sets the value of the commissionClass property.
     * 
     */
    public void setCommissionClass(int value) {
        this.commissionClass = value;
    }

    /**
     * Gets the value of the maxDailyTransfer property.
     * 
     */
    public int getMaxDailyTransfer() {
        return maxDailyTransfer;
    }

    /**
     * Sets the value of the maxDailyTransfer property.
     * 
     */
    public void setMaxDailyTransfer(int value) {
        this.maxDailyTransfer = value;
    }

    /**
     * Gets the value of the maxDailyRecharge property.
     * 
     */
    public int getMaxDailyRecharge() {
        return maxDailyRecharge;
    }

    /**
     * Sets the value of the maxDailyRecharge property.
     * 
     */
    public void setMaxDailyRecharge(int value) {
        this.maxDailyRecharge = value;
    }

    /**
     * Gets the value of the adminStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdminStatus() {
        return adminStatus;
    }

    /**
     * Sets the value of the adminStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdminStatus(String value) {
        this.adminStatus = value;
    }

    /**
     * Gets the value of the purchaseStockCommissionRate property.
     * 
     */
    public int getPurchaseStockCommissionRate() {
        return purchaseStockCommissionRate;
    }

    /**
     * Sets the value of the purchaseStockCommissionRate property.
     * 
     */
    public void setPurchaseStockCommissionRate(int value) {
        this.purchaseStockCommissionRate = value;
    }

    /**
     * Gets the value of the dealerID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDealerID() {
        return dealerID;
    }

    /**
     * Sets the value of the dealerID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDealerID(String value) {
        this.dealerID = value;
    }

    /**
     * Gets the value of the bankname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBANKNAME() {
        return bankname;
    }

    /**
     * Sets the value of the bankname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBANKNAME(String value) {
        this.bankname = value;
    }

    /**
     * Gets the value of the bankaccountno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBANKACCOUNTNO() {
        return bankaccountno;
    }

    /**
     * Sets the value of the bankaccountno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBANKACCOUNTNO(String value) {
        this.bankaccountno = value;
    }

}
