
package com.huawei.evcinterface.portalmgr;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.huawei.evcinterface.portalmgr package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.huawei.evcinterface.portalmgr
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SellStockRequest }
     * 
     */
    public SellStockRequest createSellStockRequest() {
        return new SellStockRequest();
    }

    /**
     * Create an instance of {@link DeactivateChildRequest }
     * 
     */
    public DeactivateChildRequest createDeactivateChildRequest() {
        return new DeactivateChildRequest();
    }

    /**
     * Create an instance of {@link QueryDealerInfoResult }
     * 
     */
    public QueryDealerInfoResult createQueryDealerInfoResult() {
        return new QueryDealerInfoResult();
    }

    /**
     * Create an instance of {@link ChangeLoginPwdRequest }
     * 
     */
    public ChangeLoginPwdRequest createChangeLoginPwdRequest() {
        return new ChangeLoginPwdRequest();
    }

    /**
     * Create an instance of {@link PrepaidRechargeRequest }
     * 
     */
    public PrepaidRechargeRequest createPrepaidRechargeRequest() {
        return new PrepaidRechargeRequest();
    }

    /**
     * Create an instance of {@link TransactionAuditRequest }
     * 
     */
    public TransactionAuditRequest createTransactionAuditRequest() {
        return new TransactionAuditRequest();
    }

    /**
     * Create an instance of {@link QueryTransactionRequest }
     * 
     */
    public QueryTransactionRequest createQueryTransactionRequest() {
        return new QueryTransactionRequest();
    }

    /**
     * Create an instance of {@link QueryInventoryRequest }
     * 
     */
    public QueryInventoryRequest createQueryInventoryRequest() {
        return new QueryInventoryRequest();
    }

    /**
     * Create an instance of {@link ResetChildPINRequest }
     * 
     */
    public ResetChildPINRequest createResetChildPINRequest() {
        return new ResetChildPINRequest();
    }

    /**
     * Create an instance of {@link CreateChildDealer }
     * 
     */
    public CreateChildDealer createCreateChildDealer() {
        return new CreateChildDealer();
    }

    /**
     * Create an instance of {@link QueryChildTransactionRequest }
     * 
     */
    public QueryChildTransactionRequest createQueryChildTransactionRequest() {
        return new QueryChildTransactionRequest();
    }

    /**
     * Create an instance of {@link QueryChildInventoryResult }
     * 
     */
    public QueryChildInventoryResult createQueryChildInventoryResult() {
        return new QueryChildInventoryResult();
    }

    /**
     * Create an instance of {@link QueryChildInfoRequest }
     * 
     */
    public QueryChildInfoRequest createQueryChildInfoRequest() {
        return new QueryChildInfoRequest();
    }

    /**
     * Create an instance of {@link QueryChildTransactionResult.TransactionResult }
     * 
     */
    public QueryChildTransactionResult.TransactionResult createQueryChildTransactionResultTransactionResult() {
        return new QueryChildTransactionResult.TransactionResult();
    }

    /**
     * Create an instance of {@link QueryChildInventoryRequest }
     * 
     */
    public QueryChildInventoryRequest createQueryChildInventoryRequest() {
        return new QueryChildInventoryRequest();
    }

    /**
     * Create an instance of {@link PrepaidRechargeResult }
     * 
     */
    public PrepaidRechargeResult createPrepaidRechargeResult() {
        return new PrepaidRechargeResult();
    }

    /**
     * Create an instance of {@link QueryChildInfoResult }
     * 
     */
    public QueryChildInfoResult createQueryChildInfoResult() {
        return new QueryChildInfoResult();
    }

    /**
     * Create an instance of {@link C2CTransferResult }
     * 
     */
    public C2CTransferResult createC2CTransferResult() {
        return new C2CTransferResult();
    }

    /**
     * Create an instance of {@link QueryDealerInfoRequest }
     * 
     */
    public QueryDealerInfoRequest createQueryDealerInfoRequest() {
        return new QueryDealerInfoRequest();
    }

    /**
     * Create an instance of {@link QueryTransactionResult.TransactionResult }
     * 
     */
    public QueryTransactionResult.TransactionResult createQueryTransactionResultTransactionResult() {
        return new QueryTransactionResult.TransactionResult();
    }

    /**
     * Create an instance of {@link ModifyLanguageRequest }
     * 
     */
    public ModifyLanguageRequest createModifyLanguageRequest() {
        return new ModifyLanguageRequest();
    }

    /**
     * Create an instance of {@link SellStockResult }
     * 
     */
    public SellStockResult createSellStockResult() {
        return new SellStockResult();
    }

    /**
     * Create an instance of {@link ConfirmSellStockRequest }
     * 
     */
    public ConfirmSellStockRequest createConfirmSellStockRequest() {
        return new ConfirmSellStockRequest();
    }

    /**
     * Create an instance of {@link com.huawei.evcinterface.portalmgr.TransactionAuditResult.TransactionAuditResult }
     * 
     */
    public com.huawei.evcinterface.portalmgr.TransactionAuditResult.TransactionAuditResults createTransactionAuditResultTransactionAuditResult() {
        return new com.huawei.evcinterface.portalmgr.TransactionAuditResult.TransactionAuditResults();
    }

    /**
     * Create an instance of {@link ChangePINRequest }
     * 
     */
    public ChangePINRequest createChangePINRequest() {
        return new ChangePINRequest();
    }

    /**
     * Create an instance of {@link C2CTransferRequest }
     * 
     */
    public C2CTransferRequest createC2CTransferRequest() {
        return new C2CTransferRequest();
    }

    /**
     * Create an instance of {@link com.huawei.evcinterface.portalmgr.TransactionAuditResult }
     * 
     */
    public com.huawei.evcinterface.portalmgr.TransactionAuditResult createTransactionAuditResult() {
        return new com.huawei.evcinterface.portalmgr.TransactionAuditResult();
    }

    /**
     * Create an instance of {@link QueryChildTransactionResult }
     * 
     */
    public QueryChildTransactionResult createQueryChildTransactionResult() {
        return new QueryChildTransactionResult();
    }

    /**
     * Create an instance of {@link QueryTransactionResult }
     * 
     */
    public QueryTransactionResult createQueryTransactionResult() {
        return new QueryTransactionResult();
    }

    /**
     * Create an instance of {@link QueryInventoryResult }
     * 
     */
    public QueryInventoryResult createQueryInventoryResult() {
        return new QueryInventoryResult();
    }

}
