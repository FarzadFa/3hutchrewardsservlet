
package com.huawei.evcinterface.portalmgr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PrepaidRechargeRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PrepaidRechargeRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RSMSISDN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MSISDN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="VALUE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CLIENTID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PIN" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RECHARGETYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrepaidRechargeRequest", propOrder = {
    "rsmsisdn",
    "msisdn",
    "value",
    "clientid",
    "pin",
    "rechargetype"
})
public class PrepaidRechargeRequest {

    @XmlElement(name = "RSMSISDN", required = true)
    protected String rsmsisdn;
    @XmlElement(name = "MSISDN", required = true)
    protected String msisdn;
    @XmlElement(name = "VALUE", required = true)
    protected String value;
    @XmlElement(name = "CLIENTID", required = true)
    protected String clientid;
    @XmlElement(name = "PIN", required = true)
    protected String pin;
    @XmlElement(name = "RECHARGETYPE", required = true)
    protected String rechargetype;

    /**
     * Gets the value of the rsmsisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRSMSISDN() {
        return rsmsisdn;
    }

    /**
     * Sets the value of the rsmsisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRSMSISDN(String value) {
        this.rsmsisdn = value;
    }

    /**
     * Gets the value of the msisdn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSISDN() {
        return msisdn;
    }

    /**
     * Sets the value of the msisdn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSISDN(String value) {
        this.msisdn = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVALUE() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVALUE(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the clientid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCLIENTID() {
        return clientid;
    }

    /**
     * Sets the value of the clientid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCLIENTID(String value) {
        this.clientid = value;
    }

    /**
     * Gets the value of the pin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPIN() {
        return pin;
    }

    /**
     * Sets the value of the pin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPIN(String value) {
        this.pin = value;
    }

    /**
     * Gets the value of the rechargetype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRECHARGETYPE() {
        return rechargetype;
    }

    /**
     * Sets the value of the rechargetype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRECHARGETYPE(String value) {
        this.rechargetype = value;
    }

}
